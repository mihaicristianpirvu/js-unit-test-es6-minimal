# Run unit tests on your vanilla JS project

<h2>Context: </h2>
Your vanilla-js code needs unit tests.

It was developed without NodeJS and has no ugly `X=require() syntax` but the `import {X} from './X.js'` syntax. However, we want to use jest, which is a node module. So we need to make a few adjustments. Node will be ran outside the project. We'll install babel to convert our code such that we use `require` in the unit test files, but `import/export` in our source code.

This is the smallest working example I could make. You only need to install babel & jest and have some black magic fuckery in the package.json to use babel to convert the unit test files.

<h2> How to run </h2>

```
git clone https://gitlab.com/mihaicristianpirvu/js-unit-test-es6-minimal
cd js-unit-test-es6-minimal/
npm install
npm test
```

That's it.
