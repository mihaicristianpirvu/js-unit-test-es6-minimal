// my-tests.js

const { add, multiply, MyClass } = require("../src/my-code");
// import { add, multiply } from "../src/mycode.js";

// Test the add function
test("add function", () => {
    expect(add(2, 3)).toBe(5);
    expect(add(0, 0)).toBe(0);
    expect(add(-2, 2)).toBe(0);
});

// Test the multiply function
test("multiply function", () => {
    expect(multiply(2, 3)).toBe(6);
    expect(multiply(0, 5)).toBe(0);
    expect(multiply(-2, 2)).toBe(-4);
});

// Test the multiply function
test("class method test", () => {
    const a = new MyClass();
    expect(a.try(2, 3)).toBe(6);
    expect(a.try(21, 3)).toBe(25);
});
