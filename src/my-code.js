// my-code.js

export function add(a, b) {
    return a + b;
}

export function multiply(a, b) {
    return a * b;
}

export class MyClass {
    try(a, b) {
        return a + b + 1;
    }
}
